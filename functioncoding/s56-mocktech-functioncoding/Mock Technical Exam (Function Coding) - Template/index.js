function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    if(letter.length === 1)
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    {
        for (let i = 0; i < sentence.length; i++) {
             if (sentence.charAt(i) === letter) {
               result++;
             }
           }
           return result;
    }
    // If letter is invalid, return undefined.
    else
    {
        return undefined;
    }
    
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    // Convert the text to lowercase to disregard text casing.
        text = text.toLowerCase();

        // Loop through each letter in the text and check if it appears more than once.
        for (let i = 0; i < text.length; i++) {
            for (let j = i + 1; j < text.length; j++) {
                if (text[i] === text[j]) {
                    // If a repeating letter is found, return false.
                    return false;
                }
            }
        }

        // If no repeating letters are found, return true.
        return true;
    
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
    // Return undefined for people aged below 13.
        if (age < 13) {
            return undefined;
        }
        
        // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
        if (age >= 13 && age <= 21 || age >= 65) {
            let discountedPrice = price * 0.8;
            return discountedPrice.toFixed(2);
        }
        
        // Return the rounded off price for people aged 22 to 64.
        return price.toFixed(2);

}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

      const categories = new Set(); // Use a Set to store unique categories.

      for (const item of items) {
        if (item.stocks === 0) {
          categories.add(item.category.toLowerCase()); // Add the category to the Set.
        }
      }

      return Array.from(categories); // Convert the Set to an array and return it.
}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
      // Find voters who voted for both candidate A and candidate B.
      return candidateA.filter(voter => candidateB.includes(voter));

}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};